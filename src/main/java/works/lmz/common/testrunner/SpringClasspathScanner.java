package works.lmz.common.testrunner;

import com.bluetrainsoftware.classpathscanner.ClasspathScanner;
import works.lmz.common.stereotypes.SingletonBean;

/**
 * This is an LMZ leakthrough, sometimes we need to classpath scan to get data in the right place.
 *
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
@SingletonBean
public class SpringClasspathScanner {
	public SpringClasspathScanner() {
		if ("true".equals(System.getProperty("lmz.devmode"))) {
			ClasspathScanner.getInstance().scan(Thread.currentThread().getContextClassLoader());
		}
	}
}
